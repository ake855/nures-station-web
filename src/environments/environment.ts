export const environment = {
  production: true,
  apiUrl: 'http://113.53.233.170',
  pathPrefixLookup: `:40013/lookup`,
  pathPrefixNurse:`:40012/nurse`,
  pathPrefixAuth: `:40010/auth`
};
