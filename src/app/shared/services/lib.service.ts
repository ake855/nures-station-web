import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LibService {
  pathPrefixLookup: any = `:40013/lookup`
  pathPrefixNurse: any = `:40012/nurse`
  pathPrefixAuth: any = `:40010/auth`
  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  });

  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async getWard() {
    const url = `/ward`;
    return this.axiosInstance.get(url);
  }

  async getWardName(wardId: any) {
    const url = `/ward/${wardId}`;
    return this.axiosInstance.get(url);
  }

  async getBed(wardId: any) {
    const url = `/bed/${wardId}/ward`;
    return this.axiosInstance.get(url);
  }

  async getDoctor() {
    const url = `/doctor`;
    return this.axiosInstance.get(url);
  }
}
