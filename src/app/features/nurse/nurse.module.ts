import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NurseRoutingModule } from './nurse-routing.module';
import { NurseComponent } from './nurse.component';
import { NgZorroModule } from '../../ng-zorro.module';
import { SharedModule } from '../../shared/shared.module';
import {VariableShareService} from '../../core/services/variable-share.service';


@NgModule({
  declarations: [
    NurseComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    NurseRoutingModule
  ],
  providers: [VariableShareService]
})
export class NurseModule { }
