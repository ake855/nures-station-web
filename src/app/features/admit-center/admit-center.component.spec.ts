import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmitCenterComponent } from './admit-center.component';

describe('AdmitCenterComponent', () => {
  let component: AdmitCenterComponent;
  let fixture: ComponentFixture<AdmitCenterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmitCenterComponent]
    });
    fixture = TestBed.createComponent(AdmitCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
