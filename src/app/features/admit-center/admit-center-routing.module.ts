import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmitCenterComponent } from './admit-center.component';

const routes: Routes = [{ 
  path: '', 
  component: AdmitCenterComponent,
  children: [
    {
      path: '', redirectTo: 'main', pathMatch: 'full'
    },
    {
      path: 'main', 
      loadChildren: () => import('./main/main.module').then(m => m.MainModule) 
    }
  ]
}, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmitCenterRoutingModule { }
